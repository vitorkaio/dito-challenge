import * as mongoose from 'mongoose'

const eventSchema = new mongoose.Schema({
  event: { // placa
    type: String,
    required: true
  },
  timestamp: { // chassi
    type: Date,
    required: true
  },
})

export default mongoose.model('Event', eventSchema)