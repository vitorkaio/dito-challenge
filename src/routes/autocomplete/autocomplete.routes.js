import express from 'express'
import * as AutoCompleteResolver from './autocomplete.resolver'

const router = express.Router()

router.post('/', AutoCompleteResolver.insertEvent)
router.get('/', AutoCompleteResolver.searchEvents)

export default router