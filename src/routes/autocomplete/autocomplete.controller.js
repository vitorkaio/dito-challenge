import EventAutoComplete from './autocomplete.model'

// Insere um novo evento
export const insertEvent = async (data) => {
  try {
    const res = await EventAutoComplete.create(data)
    return res
  } 
  catch (err) {
    throw (err)
  }
}


// Pega todos os eventos começados com...
export const searchEvents = async (filter) => {
  try {
    const res = await EventAutoComplete.find({ "event": { $regex: new RegExp(`^${filter}.*$`) , $options: 'i' }})
    return res
  } 
  catch (err) {
    throw (err)
  }
}