import * as AutoCompleteController from './autocomplete.controller'
import { ResponseSuccess, ResponseFail } from '../response/response'
import codes from '../response/code'
import messages from '../response/messages'


// Insere um novo event
export const insertEvent = async (req, res) => {
  try {
    const result = await AutoCompleteController.insertEvent(req.body)
    ResponseSuccess(res, codes.CREATE, result)
  } 
  catch (err) {
    responseErrors(res, err)
  }
}


// Retorna todos os eventos começados com as duas primeiras letras...
export const searchEvents = async (req, res) => {
  const { search } = req.query
  try {
    if (search && search.length <= 1) { // verifica se tem mais de 2 letras
      throw(messages.SEARCH_MIN)
    }
    const result = await AutoCompleteController.searchEvents(req.query.search)
    ResponseSuccess(res, codes.OK, result)
  } 
  catch (err) {
    ResponseFail(res, codes.ERROR, err)
  }
}
