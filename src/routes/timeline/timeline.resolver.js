import { ResponseSuccess, ResponseFail } from '../response/response'
import codes from '../response/code'
import * as TimelineController from './timeline.controller'


export const events = async (_, res) => {
  const result = await TimelineController.getAllEvents()
  if (result) {
    ResponseSuccess(res, codes.OK, result)
  }
  else ResponseFail(res, codes.ERROR, result)
}
