import express from 'express'
import * as TimelineResolver from './timeline.resolver'

const router = express.Router()

router.get('/', TimelineResolver.events)

export default router