import { getEvents } from '../../services/eventsApi'

const extractDataFields = (event) => {
  const action = {}
  event.custom_data.forEach((item) => {
    action[item.key] = item.value
  })
  return action
}

const formatComprouProdutoData = (timeline, data) => {
  const index = timeline.findIndex((item) => item.transaction_id === data.transaction_id)
  if (index === -1) {
    const transaction_id = data.transaction_id
    delete data.transaction_id
    const products = new Array({...data})
    timeline.push({transaction_id, products: [...products]})
  }
  else {
    delete data.transaction_id
    timeline[index].products.push({...data})
  }
  return [...timeline]
}


const formatComprouData = (timeline, event, data) => {
  delete event.event
  const index = timeline.findIndex((item) => item.transaction_id === data.transaction_id)
  if (index === -1) {
    timeline.push({...event, ...data, ...timeline[index]})
  }
  else {
    delete data.transaction_id
    timeline[index] = {...event, store_name: data.store_name, ...timeline[index]}
  }
  return [...timeline]
}

export const getAllEvents = async () => {
  const result = await getEvents()
  if (result) {
    const { events } = result
    let timeline = []
    for (let index = 0; index < events.length; index++) {
      if (events[index].event === 'comprou-produto') {
        const event = events[index]
        const datas = extractDataFields(event)
        // console.log(datas)
        timeline = formatComprouProdutoData([...timeline], datas)
        // console.log(timeline) 
      }
      else if (events[index].event === 'comprou') {
        const event = events[index]
        const datas = extractDataFields(event)
        delete event.custom_data
        timeline = formatComprouData([...timeline], event, datas)
      } 

    }
    return timeline.sort((a, b) => new Date(b.timestamp) - new Date(a.timestamp));
  }
  else return null
}
