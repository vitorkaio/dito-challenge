import axios from 'axios'

const url = 'https://storage.googleapis.com/dito-questions/events.json'

// Pega todos os eventos da url
export const getEvents = async () => {
  try {
    const res = await axios.get(url)
    return res.data
  } catch (_) {
    return null
  }
}
