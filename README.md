# dito-challenge

Desafio Desenvolvedor Back-End Dito.

## api está hospedada no heroku
> https://morning-coast-29677.herokuapp.com/

#### POST - insert event
- > https://morning-coast-29677.herokuapp.com/events

#### GET - autocomplete
- > https://morning-coast-29677.herokuapp.com/events?search=bu

#### GET - timeline
- > https://morning-coast-29677.herokuapp.com/timeline

## Documentação da api
> https://documenter.getpostman.com/view/1339549/SVmyQxBW?version=latest

## link do projeto
> https://gitlab.com/vitorkaio/dito-challenge/tree/master


## Executar localmente

Install packages
> yarn or npm i

Run Dev
> yarn dev

Start and Build. Gera o build e start da aplicação
> yarn start